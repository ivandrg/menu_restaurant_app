import 'package:flutter/material.dart';

class MenuPage extends StatefulWidget {
  const MenuPage({Key? key}) : super(key: key);

  @override
  State<MenuPage> createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  int _selectedIndexProduct = 0;
  int _selectedIndex = 0;

  final List<Widget> _pages = [
    // const InitialPage(),
    // const Center(child: Text('1')),
    // // const Center(child:  Text('2')),
    // const InitialPage(),
    // const Center(child: Text('3')),
  ];
  @override
  Widget build(BuildContext context) {
    double radius = 15.0;
    double heightbar = 60.0;
    final screensize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 30,
                ),
                ProfileDateWidget(),
                const SizedBox(
                  height: 30,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Best meal \nin the world',
                    style: TextStyle(
                      fontSize: 37,
                      fontWeight: FontWeight.bold,
                      letterSpacing: -1,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                SizedBox(
                  height: 60,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      CategoryButtonWidget(
                        label: 'Hamburguesas',
                        icon: 'assets/icons/taco-48.png',
                        selected: _selectedIndexProduct == 0,
                        onPressed: () {
                          setState(() {
                            _selectedIndexProduct = 0;
                          });
                        },
                      ),
                      CategoryButtonWidget(
                        label: 'Pizza',
                        icon: 'assets/icons/pizza-48.png',
                        selected: _selectedIndexProduct == 1,
                        onPressed: () {
                          setState(() {
                            _selectedIndexProduct = 1;
                          });
                        },
                      ),
                      CategoryButtonWidget(
                        label: 'Hot Dogs',
                        icon: 'assets/icons/hot-dog-48.png',
                        selected: _selectedIndexProduct == 2,
                        onPressed: () {
                          setState(() {
                            _selectedIndexProduct = 2;
                          });
                        },
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Menu',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      letterSpacing: -1,
                    ),
                  ),
                ),
                GridView.count(
                  shrinkWrap: true,
                  physics: const ScrollPhysics(),
                  childAspectRatio: 0.75,
                  crossAxisCount: 2,
                  children: const [
                    CardProductWidget(
                      label: 'Hamburgesa Doble Carne',
                      price: '11.000',
                      url:
                          'https://www.alqueria.com.co/sites/default/files/styles/1327_612/public/hamburguesa-con-amigos-y-salsa-de-champinones_0.jpg?h=2dfa7a18&itok=hLxehdIa',
                    ),
                    CardProductWidget(
                      label: 'Hamburgesa con Tocino',
                      price: '12.500',
                      url:
                          'https://www.eltiempo.com/files/article_main/uploads/2021/02/18/602efaab12112.jpeg',
                    ),
                    CardProductWidget(
                      label: 'Hamburgesa Clasica',
                      price: '9.20',
                      url:
                          'https://www.hola.com/imagenes/cocina/noticiaslibros/20210528190401/dia-internacional-hamburguesa-recetas-2021/0-957-455/adobe-burger-1-a.jpg',
                    ),
                  ],
                )
              ],
            ),
          ),
          Positioned(
            bottom: 20,
            left: screensize.width * 0.025,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(radius),
                ),
                color: const Color(0XFF131315),
              ),
              child: BottomAppBar(
                elevation: 0,
                color: Colors.transparent,
                child: SizedBox(
                  height: heightbar,
                  width: screensize.width * 0.95,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25.0, right: 25.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconBottomBar(
                            label: "",
                            icon: Icons.home,
                            selected: _selectedIndex == 0,
                            onPressed: () {
                              setState(() {
                                _selectedIndex = 0;
                              });
                            }),
                        IconBottomBar(
                            label: "",
                            icon: Icons.search_outlined,
                            selected: _selectedIndex == 1,
                            onPressed: () {
                              setState(() {
                                _selectedIndex = 1;
                              });
                            }),
                        IconBottomBar(
                            label: "",
                            icon: Icons.add_to_photos_outlined,
                            selected: _selectedIndex == 2,
                            onPressed: () {
                              setState(() {
                                _selectedIndex = 2;
                              });
                            }),
                        IconBottomBar(
                            label: "",
                            icon: Icons.local_grocery_store_outlined,
                            selected: _selectedIndex == 3,
                            onPressed: () {
                              setState(() {
                                _selectedIndex = 3;
                              });
                            }),
                        // IconBottomBar(
                        //     text: "Calendar",
                        //     icon: Icons.date_range_outlined,
                        //     selected: false,
                        //     onPressed: () {})
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // ignore: non_constant_identifier_names
  Padding ProfileDateWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: const [
              Text(
                'Sergey',
                style: TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.w600,
                  letterSpacing: -1,
                ),
              ),
              Text(
                'Dubai, Aspect Tower',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.normal,
                  letterSpacing: -1,
                  color: Colors.black54,
                ),
              ),
            ],
          ),
          const SizedBox(
            width: 10,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(10.0), //or 15.0
            child: SizedBox(
              height: 60.0,
              width: 60.0,
              // color: Color(0xffFF0E58),
              child: Image.network(
                'https://images.unsplash.com/photo-1554365228-f051bbfbcab0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class IconBottomBar extends StatelessWidget {
  const IconBottomBar(
      {Key? key,
      required this.label,
      required this.icon,
      required this.selected,
      required this.onPressed})
      : super(key: key);
  final String label;
  final IconData icon;
  final bool selected;
  final Function() onPressed;

  final primaryColor = const Color(0xFF4338CA);
  final accentColor = const Color(0xFFB59C46);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          onPressed: onPressed,
          icon: Icon(
            icon,
            size: 32,
            color: selected ? accentColor : Colors.white,
          ),
        ),
        Text(
          label,
          style: TextStyle(
            fontSize: 12,
            height: .1,
            color: selected ? accentColor : Colors.grey,
          ),
        )
      ],
    );
  }
}

class CardProductWidget extends StatelessWidget {
  const CardProductWidget({
    Key? key,
    required this.label,
    required this.price,
    required this.url,
  }) : super(key: key);
  final String label;
  final String price;
  final String url;

  @override
  Widget build(BuildContext context) {
    double _radius = 20;
    final _size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Card(
            elevation: 10,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(_radius),
            ),
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(_radius),
                    topRight: Radius.circular(_radius),
                  ),
                  child: ClipRRect(
                    child: FadeInImage(
                      placeholder: const NetworkImage(
                          'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Picture_icon_BLACK.svg/271px-Picture_icon_BLACK.svg.png'),
                      image: NetworkImage(url),
                      fit: BoxFit.fitHeight,
                      width: _size.width * .42,
                      height: 150,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 13),
                  child: Text(
                    label,
                    style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: -1,
                        overflow: TextOverflow.ellipsis),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 13),
                  child: Text(
                    '\$$price',
                    style: const TextStyle(
                      fontSize: 19,
                      fontWeight: FontWeight.bold,
                      letterSpacing: -1,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 7,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class CategoryButtonWidget extends StatelessWidget {
  const CategoryButtonWidget({
    Key? key,
    this.label,
    this.icon,
    this.selected,
    this.onPressed,
  }) : super(key: key);

  final String? label;
  final String? icon;
  final bool? selected;
  final Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              primary: Colors.white,
              shape: const StadiumBorder(),
              elevation: 7,
            ),
            icon: Image.asset(
              icon!,
              scale: 1.3,
            ),
            label: Text(
              label!,
              style: TextStyle(
                color: selected! ? Colors.black : Colors.grey,
                letterSpacing: -1,
                fontSize: 17,
              ),
            ),
            onPressed: onPressed,
          ),
        ],
      ),
    );
  }
}
